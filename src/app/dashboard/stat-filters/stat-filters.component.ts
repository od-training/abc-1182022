import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map, Subscription } from 'rxjs';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss'],
})
export class StatFiltersComponent implements OnInit, OnDestroy {
  filters: FormGroup;
  filtersSub: Subscription;

  constructor(fb: FormBuilder) {
    this.filters = fb.group({
      title: ['', Validators.minLength(3)],
      author: ['', Validators.minLength(3)],
    });

    this.filtersSub = this.filters.valueChanges
      .pipe(
        map((value) => ({
          ...value,
          title: (value.title as string).toUpperCase(),
        }))
      )
      .subscribe((value) => console.log(value));
  }

  ngOnInit(): void {}

  submit() {
    console.log('Form was submitted!', this.filters.value);
  }

  ngOnDestroy(): void {
    this.filtersSub.unsubscribe();
  }
}
