import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Observable } from 'rxjs';
import { Video } from 'src/app/app-types';
import { VideoDataService } from 'src/app/video-data.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss'],
})
export class VideoDashboardComponent implements OnInit {
  selectedVideoId: Observable<string>;
  videoList: Observable<Video[]>;

  constructor(videoSvc: VideoDataService, route: ActivatedRoute) {
    this.videoList = videoSvc.loadVideos();

    this.selectedVideoId = route.queryParamMap.pipe(
      map((params) => params.get('videoId') as string)
    );
  }

  ngOnInit(): void {}
}
